# Prueba técnica Codeoscopic

Prueba realizada por Alejandro Vañó Tomás para la empresa Codeoscopic.

## Tecnologías

- React
- Typescript

## Metodologías

- BEM

## Scripts


### `npm start`

Corre la app en modo desarrollo.
Abre [http://localhost:3000](http://localhost:3000) en el navegador.

### `npm run build`

Genera la aplicación para producción en la carpeta build

## TODO

- Poner redux
- Responsive
- Dockerizar
