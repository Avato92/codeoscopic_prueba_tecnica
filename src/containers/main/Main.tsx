import * as React from 'react';
import { Color, Response, TaskElements } from '../../models/Main';
import axiosService from '../../services/axios';
import Loading from '../../components/loading/Loading';
import Form from '../../components/form/Form';
import List from '../../components/list/List';

const Main: React.FunctionComponent<{}> = () => {
    const [colors, setColors] = React.useState<Array<Color>>([]);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [task, setTask] = React.useState<string>('')
    const [taskList, setTaskList] = React.useState<Array<TaskElements>>([]);
    const [colorSelected, setColorSelected] = React.useState<string>('fff')

    React.useEffect(() =>{
        axiosService.get<Response>('/colors/random', 10).then(
            res => {
                setColors(res.Data.colors);
                setLoading(false);
            }
        )
    },[]);

    const handleSubmit = (ev:React.FormEvent<HTMLFormElement>) => {
        let valid: boolean = false
        let id: number = 0;
        let match: Array<TaskElements> = [];
        while(!valid){
            id = Math.floor(Math.random() * 10000);
            // eslint-disable-next-line
            match = taskList.filter(task => task.id === id);
            if(match.length < 1){
                valid = true;
            } 
        }

        ev.preventDefault();
        setTaskList([...taskList, {task: task, color: colorSelected, checked: false, id: id}]);
        setTask('');
    }

    const handleOnChange = (id: string) => {
        setColorSelected(id);
    }

    const handleOnChangeInput = (txt: string) => {
        setTask(txt);
    }

    const handleCheckbox = (id: number) => {
        const taskListChanged = taskList.map(task => {
            if(task.id === id){
                task.checked = !task.checked;
            }
            return task;
        });
        setTaskList(taskListChanged);
    }

    const deleteTask = (id: number) => {
        const deletedTask = taskList.filter(task => task.id !== id);
        setTaskList(deletedTask);
    }

    return (
        <div>
            {
                loading?
                    <Loading />
                :
                (
                    <div>
                        <Form 
                            colors={colors} 
                            task={task} 
                            handleSubmit={handleSubmit} 
                            handleOnChange={handleOnChange} 
                            handleOnChangeInput={handleOnChangeInput}
                            colorSelected={colorSelected}
                             />
                        <List 
                            taskList={taskList}
                            handleCheckbox={handleCheckbox}
                            deleteTask={deleteTask} 
                        />
                    </div>
                )
            }
            
        </div>
    )
}

export default Main;