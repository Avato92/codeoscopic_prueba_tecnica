import * as React from 'react';
import { Color } from '../../models/Main';
import './Form.css';

interface FormProps {
    colors: Array<Color>;
    task: string;
    colorSelected: string;
    handleSubmit: (ev:React.FormEvent<HTMLFormElement>) => void;
    handleOnChange: (id:string) => void;
    handleOnChangeInput: (text:string) => void;
}
const Form: React.FunctionComponent<FormProps> = (props: FormProps) => {
    const {colors, task, colorSelected, handleSubmit, handleOnChange, handleOnChangeInput} = props;

    return (
        <div className='form'>
            <h1>Simple TODO List App:</h1>
            <form onSubmit={(ev) => handleSubmit(ev)} className='form__container'>
                <label className='form__label'>
                    Task:
                    <input key='task' className='form__input' value={task} onChange={(ev) => handleOnChangeInput(ev.target.value)} />
                </label>
                <label className='form__label'>
                    Colour:
                    <select style={{backgroundColor: '#' + colorSelected}} onChange={(ev) => handleOnChange(ev.target.value)} className='form__select'>
                        <option value='fff' className='form__option'>None</option>
                            {colors.map( color => {
                                return (
                                    <option 
                                        className='form__option'
                                        style={{backgroundColor: '#' + color.hex}} 
                                        key={color.id} 
                                        value={color.hex}>{color.tags? color.tags.map(tag => tag.name + ' '): color.hex}
                                    </option>
                                );
                            })}
                    </select>
                </label>
                <input type='submit' value='Submit' className='form__submit' />
            </form>
        </div>
    )
}

export default Form;