import * as React from 'react';
import { FaTrashAlt } from 'react-icons/fa';
import { TaskElements } from '../../models/Main';
import './List.css';

interface ListProps {
    taskList: Array<TaskElements>;
    handleCheckbox: (id: number) => void;
    deleteTask: (id: number) => void;
}

const List: React.FunctionComponent<ListProps> = (props: ListProps) => {
    const {taskList, handleCheckbox, deleteTask} = props;

    return (
       <div className='list'>
           <h2 className='list__title'>Task list:</h2>
           {taskList.length < 1? 
                <h3>No tasks</h3>
            :
            <ul className='list__container'>
                {taskList.map(({task, color, checked, id}) => {
                    return(
                        <li style={{backgroundColor: '#' + color}} className='list__items'>
                            <div className='list__element'>
                                <input type='checkbox' checked={checked} className='list__check' key={id} onChange={() => handleCheckbox(id)} />
                                <label className={checked?'list__label list__label-line-througth':'list__label'}>{task}</label>
                            </div>
                            <button className='list__delete' style={{backgroundColor: '#' + color}} onClick={() => deleteTask(id)}><FaTrashAlt /></button>
                        </li>
                    )
                })}
            </ul>
           }
       </div>
    )
}

export default List;