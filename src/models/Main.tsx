type Tag = {
    timestamp: number,
    id: number,
    name: string
};

export type Color = {
    timestamp: number,
    hex?: string,
    id: number,
    tags?: Array<Tag>
};

export type Response = {
    colors: Array<Color>,
    schemes?: Array<any>,
    schemes_history?: Object,
    success: boolean,
    colors_history: any,
    messages?: Array<any>
}

export type TaskElements = {
    task: string,
    color: string,
    checked: boolean,
    id: number
}